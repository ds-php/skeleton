<?php

namespace Ds\App;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Ds\App\Exceptions\RespondException;
use Ds\App\Log\NullLogger;
use Ds\Middleware\Pipe;
use Ds\Middleware\PipeInterface;
use Ds\Middleware\Stack;
use Ds\Middleware\StackInterface;
use Ds\Router\Dispatcher\Dispatcher;
use Ds\Router\Interfaces\LoaderInterface;
use Ds\Router\Interfaces\RouterInterface;
use Ds\Router\Interfaces\RouterResponseInterface;
use Zend\Diactoros\Response;

/**
 * Main App.
 *
 * Main application class.
 *
 * @package Ds\App
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://red-sqr.co.uk/Framework/Skeleton/wikis/
 * @see     AppFactory For Application Creation.
 * @see     AppBuilder For Bootstraped Application Creation.
 */
class App implements AppInterface
{
    /**
     * Termination Header Constant
     */
    const TERMINATE = 'APP_TERMINATE';

    /**
     * Early Middleware Stack Name.
     */
    const EARLY = 'early';

    /**
     * Middleware Stack Name.
     */
    const MIDDLEWARE = 'middleware';

    /**
     * IoC Container
     *
     * @var ContainerInterface $container
     */
    public $container;

    /**
     * Router
     *
     * @var RouterInterface $router
     */
    public $router;

    /**
     * PSR7 Middleware Pipe
     *
     * @var PipeInterface $middleware
     */
    public $middleware;

    /**
     * PSR Logger
     *
     * @var LoggerInterface $log
     */
    public $log;

    /**
     * Application constructor.
     *
     * @param RouterInterface $router Router
     * @param ContainerInterface $container Container
     */
    public function __construct(
        RouterInterface $router,
        ContainerInterface $container
    )
    {
        $this->router = $router;
        $this->container = $container;
        $this->middleware = new Pipe();
        $this->log = new NullLogger();
        $this->stack = new Stack();
    }

    public static function getConstants($dir)
    {
        $dev = 'dev-';
        $filename = 'constants.php';
        $file = $dir . DIRECTORY_SEPARATOR . \ltrim($filename, '/');
        if (\file_exists($file)) {
            require_once $file;
        } else {
            $devFile = $dir . DIRECTORY_SEPARATOR . \ltrim($dev . $filename, '/');
            if (\file_exists($devFile)) {
                require_once $devFile;
            }
        }
    }

    /**
     * Add Termination Header to response.
     *
     * @param ResponseInterface $response Response.
     *
     * @return ResponseInterface
     */
    public static function terminate(ResponseInterface $response): ResponseInterface
    {
        return $response->withHeader('APP_TERMINATE', true);
    }

    /**
     * Replace App Middleware Stack.
     *
     * @param StackInterface $stack Middleware Stack.
     *
     * @return AppInterface
     */
    public function withStack(StackInterface $stack): AppInterface
    {
        $new = clone $this;
        $new->stack = $stack;
        return $new;
    }

    /**
     * Replace App Logger.
     *
     * @param LoggerInterface $log PSR Logger.
     *
     * @return AppInterface
     */
    public function withLogger(LoggerInterface $log): AppInterface
    {
        $new = clone $this;
        $new->log = $log;
        return $new;
    }

    /**
     * Load App routes.
     *
     * @param LoaderInterface $loader Router Loader.
     * @param array $files Route Files.
     *
     * @return AppInterface
     */
    public function loadRoutes(LoaderInterface $loader, array $files = []): AppInterface
    {
        $new = clone $this;
        $new->router = $loader->loadFiles($files);
        return $new;
    }

    /**
     * Handle Request.
     *
     * @param ServerRequestInterface $request Server Request
     * @param bool $throw Throw Exceptions
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function handle(
        ServerRequestInterface $request,
        bool $throw = true
    ): ResponseInterface
    {
        $this->middleware = $this->middleware->withContainer($this->container);
        $routerResponse = $this->router->getRouterResponse($request);

        foreach ($routerResponse->getVars() as $key => $param){
            $request = $request->withAttribute($key,$param);
        }

        $response = $this->createHandlerResponse(
            new Response(),
            $request,
            $routerResponse
        );

        try {
            $earlyPipe = $this->getMiddlewarePipe(self::EARLY, $routerResponse->getNames());
            $response = $earlyPipe->execute($request, $response);
            $request = $earlyPipe->getRequest();

            //Update the RouterResponse with Early Middleware Response before dispatching.
            $routerResponse = $routerResponse->withHandler(
                $response->getBody()->getContents()
            );

            if ($response->hasHeader(self::TERMINATE)) {
                $this->log->notice("App Terminated: {$request->getRequestTarget()}");
                return $response;
            }

            $middlewarePipe = $this->getMiddlewarePipe(self::MIDDLEWARE, $routerResponse->getNames());

            $controllerArgs = [
                'container' => $this->container,
                'response' => $response
            ];

            $response = $middlewarePipe->execute(
                $request,
                $this->dispatchHandler(
                    $request,
                    $routerResponse,
                    $controllerArgs
                )
            );

        } catch (\Exception $e) {
            $this->log->critical($e->getMessage());

            if ($throw) {
                throw new RespondException($e->getMessage());
            }
        }
        return $response;
    }

    /**
     * Create Response with handler handler.
     *
     * @param ResponseInterface $response Response
     * @param ServerRequestInterface $request Sever Request
     * @param RouterResponseInterface $routerResponse Router Response
     *
     * @return ResponseInterface
     */
    public function createHandlerResponse(
        ResponseInterface $response,
        ServerRequestInterface $request,
        RouterResponseInterface $routerResponse
    ): ResponseInterface
    {
        $handler = $routerResponse->getHandler();

        if (!is_string($handler)) {
            $handler = $handler($request);
        }

        $body = $response->getBody();
        $body->write($handler);
        $body->rewind();

        $response = $response
            ->withStatus($routerResponse->getStatusCode())
            ->withBody($body);

        return $response;
    }

    /**
     * Return Middleware Pipe
     * @param string $name
     * @param array $routeNames
     *
     * @return PipeInterface
     */
    private function getMiddlewarePipe(string $name, array $routeNames)
    {
        return $this->middleware->fromStack(
            $this->stack,
            $name,
            $routeNames
        );
    }

    /**
     * Resolve handler and dispatch Response.
     *
     * @param ServerRequestInterface $request Server Request.
     * @param RouterResponseInterface $routerResponse Router response.
     * @param array $constructor Request. Vars to be available to routes.
     *
     * @return ResponseInterface
     */
    public function dispatchHandler(
        ServerRequestInterface $request,
        RouterResponseInterface $routerResponse,
        array $constructor
    ): ResponseInterface
    {
        $dispatcher = new Dispatcher(
            ['autoWire' => true]
        );

        foreach ($request->getAttributes() as $type => $attr) {
            switch ($type) {
                case 'redirect':
                    $rResponse = $this->router->getRouterResponseFromPath($attr[0], $attr[1]);
                    if ($rResponse->getStatusCode() === 200) {
                        $routerResponse = $rResponse;
                    }
                    break;
            }
        }

        return $dispatcher->dispatch(
            $request,
            $routerResponse->getHandler(),
            $constructor
        );
    }

    /**
     * Process ResponseInterface Headers and output Body Stream.
     *
     * @param ResponseInterface $response Response Object
     * @param bool $displayHeaders Output Headers.
     * @param bool $flush Flush Screen.
     *
     * @return void
     */
    public function respond(
        ResponseInterface $response,
        bool $displayHeaders = true,
        bool $flush = true
    )
    {
        if ($flush === true) {
            \ob_clean();
            \ob_start();
        }

        if ($displayHeaders === true) {
            \http_response_code($response->getStatusCode());
            $headers = $response->getHeaders();
            foreach ($headers as $header => $value) {
                \header($header . ': ' . $response->getHeaderLine($header));
            }
        }

        echo $response->getBody()->getContents();

        if ($flush === true) {
            \ob_end_flush();
        }
    }

    /**
     * Display offline html page with 500 internal server error.
     *
     * @param string $page HTML page to be displayed.
     * @param bool $die    Kill the application
     */
    public static function offlineHtml($page, $die = true){
        \header("HTTP/1.0 500 Internal Server Error");

        if (\file_exists($page)){
            include($page);
        }

        if ($die){
            die();
        }
    }
}
