<?php

namespace Ds\App;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Ds\App\Builder\AppBuilder;
use Ds\Middleware\StackInterface;
use Ds\Router\Interfaces\LoaderInterface;
use Ds\Router\Interfaces\RouterResponseInterface;

/**
 * AppInterface.
 *
 * @package Ds\App
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://red-sqr.co.uk/Framework/Skeleton/wikis/
 *
 * @see     AppFactory For Application Creation.
 * @see     AppBuilder For Bootstraped Application Creation.
 */
interface AppInterface
{
    /**
     * Add Termination Header to response.
     *
     * @param ResponseInterface $response Response.
     *
     * @return ResponseInterface
     */
    public static function terminate(ResponseInterface $response): ResponseInterface;

    /**
     * Resolve handler and dispatch Response.
     *
     * @param ServerRequestInterface $request Server Request.
     * @param RouterResponseInterface $routerResponse Router response.
     * @param array $constructor Request. Vars to be available to routes.
     *
     * @return ResponseInterface
     */
    public function dispatchHandler(
        ServerRequestInterface $request,
        RouterResponseInterface $routerResponse,
        array $constructor
    ): ResponseInterface;

    /**
     * Handle Request.
     *
     * @param ServerRequestInterface $request Server Request
     * @param bool $throw Throw Exceptions
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function handle(ServerRequestInterface $request, bool $throw = true
    ): ResponseInterface;

    /**
     * Process ResponseInterface Headers and output Body Stream.
     *
     * @param ResponseInterface $response Response Object
     * @param bool $displayHeaders Output Headers.
     * @param bool $flush Flush Screen.
     *
     * @return void
     */
    public function respond(
        ResponseInterface $response,
        bool $displayHeaders = true,
        bool $flush = true
    );
}
