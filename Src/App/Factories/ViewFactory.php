<?php

namespace Ds\App\Factories;

use Ds\View\ViewInterface;
use Ds\View\Viewer;
use Ds\View\TemplateEngine\TwigEngine;

/**
 * Class ViewFactory
 * @package Ds\App\Factories
 */
class ViewFactory
{
    /**
     * Default Factory Values.
     *
     * @var array $defaults
     */
    public static $defaults = [

        'options' => [
            'debug' => true,
            'charset' => 'utf-8',
            'base_template_class' => 'Twig_Template',
            'cache' => false,
            'auto_reload' => true
        ],

        'templateDir' => ROOT . '/View',
        'themes' => [],
        'extensions' => [],
        'globals' => []
    ];

    /**
     * Create New View
     *
     * @param array $options View Options
     *
     * @return ViewInterface
     */
    public static function create(array $options = [])
    {

        $options = \array_replace_recursive(ViewFactory::$defaults,$options);
        $twigLoader = new \Twig_Loader_Filesystem($options['templateDir']);
        $twigEnvironment = new \Twig_Environment($twigLoader, $options['options']);

        foreach ((array)$options['extensions'] as $extension) {
            if (\class_exists($extension)) {
                $twigEnvironment->addExtension(new $extension());
            }
        }

        foreach ((array)$options['themes'] as $name => $directory) {
            $twigLoader->addPath($directory, $name);
        }

        foreach ((array)$options['globals'] as $globalName => $globalValue) {
            $twigEnvironment->addGlobal($globalName, $globalValue);
        }

        return new Viewer(
            new TwigEngine(
                $twigLoader,
                $twigEnvironment
            ),
            CacheFactory::createMemcache()
        );
    }
}
