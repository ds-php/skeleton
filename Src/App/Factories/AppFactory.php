<?php

namespace Ds\App\Factories;

use DI\ContainerBuilder;
use Interop\Container\ContainerInterface;
use Ds\App\App;
use Ds\Router\RouterFactory;

/**
 * Class AppFactory
 *
 * @package Ds\App\Builder
 */
class AppFactory
{
    /**
     * @var array $defaults Application default settings.
     */
    public static $defaults = [
        'router' => [
            'cacheDisabled' => true,
            'cacheFile' => __DIR__ . 'r.cache',
            'errorHandlers' => [
                'default' => [
                    'handler' => 'ErrorController::error404',
                    'name' => ['error']
                ]
            ]
        ],
        'container' => [
            'useAnnotations' => false,
            'useAutowiring' => true,
            'writeProxiesToFile' => true,
            'proxyFile' => 'tmp/proxies',
            'definitions' => []
        ]
    ];


    /**
     * Return application defaults.
     *
     * @return array
     */
    public static function getDefaults()
    {
        return (array)self::$defaults;
    }

    /**
     * Create new Application from options.
     *
     * @param array $options Application settings.
     *
     * @return App
     */
    public static function create(array $options = [])
    {
        $options = \array_replace_recursive(self::$defaults, $options);

        return new App(
            RouterFactory::createFastRouter($options['router']),
            self::_createContainer($options['container'])
        );
    }

    /**
     * Create DI Container from DI\ContainerBuilder
     *
     * @param array $options
     *
     * @return ContainerInterface
     */
    protected static function _createContainer(array $options = [])
    {
        $options += self::$defaults;
        $builder = new ContainerBuilder();
        $builder->useAnnotations($options['useAnnotations']);
        $builder->useAutowiring($options['useAutowiring']);
        $builder->writeProxiesToFile(
            $options['writeProxiesToFile'], $options['proxyFile']
        );
        foreach ((array)$options['definitions'] as $def) {
            $builder->addDefinitions($def);
        }
        return $builder->build();
    }
}
