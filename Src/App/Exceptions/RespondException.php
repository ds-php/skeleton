<?php
namespace Ds\App\Exceptions;

/**
 * Class RespondException
 * @package Ds\App\Exceptions
 */
class RespondException extends \Exception
{
}
