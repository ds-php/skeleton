<?php

namespace Demo\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Ds\App\Controller\Controller;

/**
 * Class ExampleController
 */
class ExampleController extends Controller
{
    /**
     * @param ServerRequestInterface $request
     * @return string
     */
    public function bar(ServerRequestInterface $request)
    {
        return $this->render('home/index.twig', [], ['cache' => false]);
    }
}
