<?php

namespace Demo\Bootstraps;

use Demo\Middleware\TrackingCode\TrackingCode;
use Ds\App\Bootstrap\FastRouteBootstrap;

/**
 * Class Bootstrap
 *
 * @package Demo\Bootstraps
 */
class Bootstrap extends FastRouteBootstrap
{
    /**
     * @inheritdoc
     */
    public function getOptions()
    {
        return [
            'router' => [
                'cacheDisabled' => true,
                'cacheFile' => __DIR__ . 'r.cache',
                'errorHandlers' => [
                    'default' => [
                        'handler' => 'Demo\Controllers\ErrorController::error500',
                        'name' => ['error']
                    ],
                    '404' => [
                        'handler' => 'Demo\Controllers\ErrorController::error404',
                        'name' => ['error']
                    ],
                    '405' => [
                        'handler' => 'Demo\Controllers\ErrorController::error405',
                        'name' => ['error']
                    ]
                ]
            ],
            'container' => [
                'useAnnotations' => false,
                'useAutowiring' => true,
                'writeProxiesToFile' => true,
                'proxyFile' => 'tmp/proxies',
                'definitions' => [ROOT . '/Demo/Definitions/definitions.php'],
                'declarations' => [ROOT . '/Demo/Declarations/declarations.php']
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function getRoutes()
    {
        return [
            'php' => [
                ROOT . '/Demo/Routes/routes.php'
            ]
        ];
    }

    public function getEarly()
    {
        return [
        ];
    }

    public function getMiddleware()
    {
        return [
            [TrackingCode::create('foo'), ['global', 'name']]
        ];
    }
}
