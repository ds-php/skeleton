<?php

namespace Demo\Middleware\TrackingCode;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Ds\App\Middleware;

/**
 * Google Tracking Code.
 *
 * Updated Response and replaces '(gacode)(/gacode)' with Tracking Code.
 *
 * @package MA\Middleware\GA
 */
class TrackingCode extends Middleware
{

    /**
     * Regex String.
     */
    const REGEX = '/\(gacode\)(.*?)\(\/gacode\)/';

    /**
     * Tracking Code Ident
     *
     * @var string
     */
    private $code;


    /**
     * Create Tracking Code object with Tracking Ident
     *
     * @param string $code Tracking Code Ident
     *
     * @return TrackingCode
     */
    public static function create(string $code): TrackingCode
    {
        $ga = new TrackingCode(null);
        $ga->withCode($code);
        return $ga;
    }

    /**
     * Update middleware tracking code ident.
     *
     * @param string $code Tracking Code Ident
     */
    public function withCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * Search for (gacode)(/gacode) and replace with javascript tracking code.
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param callable|null $next Next Middleware
     * @return ResponseInterface
     * @throws \InvalidArgumentException
     */
    public function __invoke(
        RequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ): ResponseInterface
    {
        if ($next) {
            $response = $next($request, $response);
        }

        $currentBody = $response->getBody();
        $bodyContent = $currentBody->getContents();

        $subst = $this->createTrackingCode($this->code);
        $newBody = preg_replace(TrackingCode::REGEX, $subst, $bodyContent);

        $body = new \Zend\Diactoros\Stream("php://memory", "wb+");
        $body->write($newBody);
        $body->rewind();

        $response = $response->withBody($body);

        return $response;
    }

    /**
     * Generate tracking code
     * @param $code string Tracking Code Ident
     * @return string
     */
    public function createTrackingCode($code): string
    {
        return <<<SCRIPT
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', '$code', 'auto');ga('send', 'pageview');
SCRIPT;
    }
}
