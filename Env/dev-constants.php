<?php
/**
 * @var string STAGE Staging Environment
 *
 * DEV : local vagrant http://app.dev
 * STAGING: staging environment http://staging.martin-audio.com
 * RELEASE main site https://martin-audio.com
 */
define("STAGE", 'DEV');

/**
 * @var string DEPLOYMENT Asset Subdirectory
 */
define("DEPLOYMENT", '/Staging');

/**
 * @var string VERSION JS/CSS Cache Busting
 */
define("VERSION", '?v1.0.17');

/**
 * Template Constants.
 * @var string DOMAIN Domain Name
 * @var string ASSETS Static Resources
 * @var string DOWNLOADS Download Resources
 *
 */
define("DOMAIN", 'http://app.dev');
define("ASSETS", 'https://app.dev');
define("DOWNLOADS", 'https://app.dev/downloads');
