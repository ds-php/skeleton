#!/bin/bash

CreateNewHost(){

local vP1="$"
local vP2="1"
local HOSTSTRING="RewriteRule ^(.*) public/$vP1$vP2 [L]"

sudo cat >> /etc/apache2/sites-enabled/${1}.conf <<-EOF
<VirtualHost *:80>
        ServerName www.${2}
        ServerAlias ${2}

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/${1}

        ErrorLog /var/log/apache2/${3}
        CustomLog /var/log/apache2/${4} combined

        <Directory "/var/www/${1}">
            RewriteEngine On
            RewriteCond %{REQUEST_URI} !^/Assets/.*$
            ${HOSTSTRING}
        </Directory>

        <Directory "/var/www/${1}/public">
            RewriteEngine On
            RewriteCond %{REQUEST_FILENAME} !-d
            RewriteCond %{REQUEST_FILENAME} !-f
            RewriteRule ^ index.php [QSA,L]
        </Directory>

        #Include conf-available/serve-cgi-bin.conf
</VirtualHost>
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
EOF
}


CreateNewHost "Application" "App.dev" "AppError.log" "AppAccess.log"
sudo service apache2 restart

