#!/bin/bash


#basic tools and set-up
sudo apt-get install -y htop apache2 php7.0 libapache2-mod-php7.0 php-memcached memcached php-xdebug

#enable any apache2 mods (requires restart of apache service)
sudo a2enmod rewrite
sudo a2enmod expires

#mysql database - user : root / password : password
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password password'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password password'
sudo apt-get -y install mysql-server
sudo apt-get -y install php7.0-gd php7.0-mysql php7.0-xml php7.0-dom npm nodejs-legacy

#update permissions on synced profiler folder
sudo chmod go+w /var/profiler

#enable xdebug
sudo cat > /etc/php/7.0/apache2/conf.d/20-xdebug.ini <<-EOF
zend_extension=xdebug.so
xdebug.remote_enable = on
xdebug.remote_connect_back = on
always_populate_raw_post_data=-1
xdebug.profiler_enable_trigger = 1
xdebug.profiler_append=1
xdebug.profiler_output_dir ="/var/profiler/"
xdebug.profiler_output_name = 'cachegrind.out.%p'

EOF

#set up a basic firewallcd /e
sudo apt-get install -y ufw
sudo ufw allow www
sudo ufw allow mysql
sudo ufw allow ssh
sudo ufw allow 1121 #memcache
sudo ufw allow 9000 #xdebug connections

#install composer and make it globally available
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

#restart any services
sudo service ufw start
sudo service apache2 restart

