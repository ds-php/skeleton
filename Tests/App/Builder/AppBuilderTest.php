<?php

namespace Tests\Ds\App\Builder;

use Ds\App\Bootstrap\AbstractBootstrap;
use Ds\App\Builder\AppBuilder;
use Ds\App\Exceptions\BuilderException;
use Tests\Ds\App\Bootstrap\MockBootstrap;


class DefaultMockBootstrap extends AbstractBootstrap{
    public function getOptions()
    {
        return [
            'option' => true
        ];
    }

    public function getRoutes()
    {
        return [
            'routes' => true
        ];
    }

    public function getEarly()
    {
        return [
            'early' => true
        ];
    }

    public function getMiddleware()
    {
        return [
            'middleware' => true
        ];
    }
}

/**
 * Class AppBuilderTest
 * @package Tests\Ds\App\Builder
 */
class AppBuilderTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var string
     */
    public $deploymentFlag;

    /**
     * @var AppBuilder
     */
    public $builder;

    /**
     * Test exception thrown with no bootstrap.
     */
    public function testGetBootstrapException()
    {
        $this->expectException(BuilderException::class);
        $this->builder->getBootstrap();
    }


    /**
     * Test that boo
     */
    public function testWithBootstrap()
    {
        $bootstrap = new MockBootstrap();
        $builder = $this->builder->withBootstrap('dev', $bootstrap);
        $this->assertSame($bootstrap, $builder->getBootstrap());
    }

    public function testGetAppOptions(){

        $builder = $this->builder->withBootstrap('dev',new DefaultMockBootstrap());
        $expected = [
            'options' => ['option' => true],
            'routes' => ['routes' => true],
            'early' => ['early' => true],
            'middleware' => ['middleware' => true]
        ];
        $this->assertSame($expected, $builder->getAppOptions());
    }

    /**
     *
     */
    protected function setUp() : void
    {
        $this->deploymentFlag = 'dev';
        $this->builder = new AppBuilder($this->deploymentFlag, [
            'cache' => false
        ]);
    }
}
