<?php

namespace Tests\Ds\App\Bootstrap;

use Ds\App\Bootstrap\AbstractBootstrap;

/**
 * Class MockBootstrap
 *
 * @package Tests\Ds\App\Bootstrap
 */
class MockBootstrap extends AbstractBootstrap
{
    public function getOptions()
    {
        return [
            'option' => true
        ];
    }

    public function getRoutes()
    {
        return [
            'routes' => true
        ];
    }

    public function getEarly()
    {
        return [
            'early' => true
        ];
    }

    public function getMiddleware()
    {
        return [
            'middleware' => true
        ];
    }

}


/**
 * Class AbstractBootstrapTest
 * @package Tests\Ds\App\Bootstrap
 */
class AbstractBootstrapTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var MockBootstrap
     */
    public $bootstrap;

    /**
     *
     */
    public function testGetOptions()
    {
        $expected = ['option' => true];
        $actual = $this->bootstrap->getOptions();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testGetRoutes()
    {
        $expected = ['routes' => true];
        $actual = $this->bootstrap->getRoutes();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testGetEarly()
    {
        $expected = ['early' => true];
        $actual = $this->bootstrap->getEarly();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testGetMiddleware()
    {
        $expected = ['middleware' => true];
        $actual = $this->bootstrap->getMiddleware();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    protected function setUp() : void
    {
        $this->bootstrap = new MockBootstrap();
    }
}
