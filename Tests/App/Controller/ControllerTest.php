<?php
namespace Tests\Ds\App\Controller;

use Ds\View\ViewInterface;
use Interop\Container\ContainerInterface;
use Ds\App\Controller\Controller;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Stream;

/**
 * Class ControllerTest
 * @package Tests\Ds\App\Controller
 */
class ControllerTest extends \PHPUnit\Framework\TestCase
{
    public $serverRequest;
    public $response;
    public $view;

    /**
     * @var Controller
     */
    public $controller;
    public $container;



   public function testGetResponse(){
        $response = $this->controller->getResponse();
        $this->assertSame($response, $this->response);
   }



   public function testRender(){

       $template = 'my-template.twig.html';
       $data = ['foo' => 'bar'];
       $options = ['cache' => true];

       $expected = \json_encode([
           'template' => $template,
           'data' => $data,
           'options' => $options
       ]);

       $this->response->expects($this->once())
           ->method('withBody')
           ->willReturn($this->response);

       $this->controller->render($template,$data,$options);
   }

    public function testRenderView(){}
    public function testCreateStream(){}
    public function testRedirectResponse(){}

    protected function setUp() : void
    {
        $this->view = $this->getMockBuilder(ViewInterface::class)->getMock();
        $this->container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $this->serverRequest = $this->getMockBuilder(ServerRequestInterface::class)->getMock();
        $this->response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $this->controller = new Controller($this->container, $this->response, ['cache' => false]);
    }
}