<?php

namespace Tests\Ds\App;

use Ds\App\App;
use Ds\Router\Router;
use Interop\Container\ContainerInterface;

use Zend\Diactoros\Response;

class AppTest extends \PHPUnit\Framework\TestCase
{

    public $app;

    public function testRespond()
    {

        $response = new Response();

        $body = $response->getBody();
        $body->write('helloworld');
        $body->rewind();

        $response = $response->withBody($body);

        ob_start();
        $this->app->respond($response, false);
        $out1 = ob_get_contents();
        ob_end_clean();
        $this->assertEquals($out1, 'helloworld');
    }


    protected function setUp() : void
    {
        $router = $this->getMockBuilder(Router::class)
            ->disableOriginalConstructor()
            ->getMock();

        $container = $this->getMockBuilder(ContainerInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->app = new App(
            $router,$container
        );
    }
}
